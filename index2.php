<!DOCTYPE html>
<html>
<head>
	<title>Electric World</title>

	<!-- Bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cyborg/bootstrap.css">

	<!-- Font Style -->
    <link href="https://fonts.googleapis.com/css?family=Faster+One&display=swap" rel="stylesheet">

    <!-- CSS Link -->
    <link rel="stylesheet" type="text/css" href="assets/styles/style.css">

</head>
<body>
	<header>
		<!-- Navbar from Bootswatch -->
		<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
		  <a class="navbar-brand" href="#">Electric World</a>
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>

		  <div class="collapse navbar-collapse" id="navbarColor02">
		    <ul class="navbar-nav mr-auto">
		      <li class="nav-item active">
		        <a class="nav-link" href="#">Product List <span class="sr-only">(current)</span></a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">Add Item</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">Cart</a>
		      </li>
		      <li class="nav-item">
		        <a class="nav-link" href="#">About</a>
		      </li>
		    </ul>
		  </div>
		</nav>
		<div class="d-flex justify-content-center align-items-center flex-column" style = "height: 75vh">
			<h1 class="text-center">Welcome to<br><span>Electric World!</span></h1>
			<a href="views/catalog.php" class="btn btn-danger my-4">View Menu</a>
		</div>
	</header>

	<!-- Featured Products Page -->
	<section>
		<h1 class="text-center p-5">Featured Electric Scooters</h1>
		<div class="container">
			<div class="row">
				<?php
					$products = file_get_contents("assets/lib/products.json");
					// var_dump($products);
					$products_array = json_decode($products, true);
					// var_dump($products_array);
					for($i=0; $i<count($products_array); $i++){
						if(isset($products_array[$i])){
				?>
					<div class="col-lg-4 py-2">
						<div class="card">
							<img src="assets/lib/<?php echo $products_array[$i]["image"]?>" class="card-img-top" height="450px" alt="">
							<div class="card-body">
								<h5 class="card-title"><?php echo $products_array[$i]['name']?></h5>
								<p class="card-text">Price: Php<?php echo $products_array[$i]['price']?></p>
								<p class="card-text">Description: <?php echo $products_array[$i]['description']?></p>
							</div>
						</div>
					</div>
				<?php
					}
				}
				?>
			</div>
		</div>
	</section>
</body>
</html>