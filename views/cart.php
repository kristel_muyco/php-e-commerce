<?php
	require "../partials/template.php";

	function get_body_contents(){
?>
	<h1 class="text-center py-3">Cart Page</h1>
	<hr>
	<div class="container">
		<div class="row">
			<div class="col-lg-8 offset-lg-2">
				<table class="table table-striped">
					<thead>
						<th>Item Name:</th>
						<th>Item Price:</th>
						<th>Item Quantity:</th>
						<th>Subtotal</th>
						<th></th>
					</thead>
					<tbody>
						<?php

							$products = file_get_contents("../assets/lib/products.json");

							$products_array = json_decode($products, true);

							$total = 0;

							if(isset($_SESSION['cart'])){
								foreach($_SESSION['cart'] as $name => $quantity){
									foreach ($products_array as $indiv_product){
										if($name == $indiv_product['name']){
											$Subtotal = $indiv_product['price']*$quantity;
											$total += $Subtotal;
									?>
								<tr>
									<td><?php echo $name ?></td>
									<td><?php echo number_format($indiv_product['price'], 2, ".", ",") ?></td>
									<td>
										<form action="../controllers/add-to-cart-process.php" method="POST">
											<input type="hidden" name="name" value="<?php echo $name ?>">
											<input type="hidden" name="fromCartPage" value="fromCartPage">
											<div class="input-group">
												<input class="form-control" type="number" name="quantity" value="<?php echo $quantity?>">
												<div class="input-group-append">
												<button class="btn btn-sm btn-success" type="submit">Update</button>	
												</div>
											</div>
										</form>
									</td>
									<td><?php echo number_format($Subtotal, 2, ".", ",") ?></td>
									<td><a href="../controllers/remove-from-cart-process.php?name=<?php echo $name ?>" class="btn btn-danger">Remove from Cart</a></td>
								</tr>
									<?php
										}  
								
									}
								}
							}
						?>
						<tr class="bg-danger">
							<td></td>
							<td></td>
							<td>Total:</td>
							<td><?php echo number_format($total, 2, ".", ",") ?></td>
							<td><a href="../controllers/empty-cart-process.php" class="btn btn-danger">Empty Cart</a></td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
<?php		
	}

?>